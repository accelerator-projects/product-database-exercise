package Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import Model.ProductTransformer;
import Model.json.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDatabase {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private ProductTransformer productTransformer;

    @Autowired
    public ProductDatabase(final NamedParameterJdbcTemplate jdbcTemplate, final ProductTransformer productTransformer) {
        this.jdbcTemplate = jdbcTemplate;
        this.productTransformer = productTransformer;
    }

    public void createProduct(Model.domain.Product product) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", product.getId())
                .addValue("name", product.getName())
                .addValue("description", product.getDescription())
                .addValue("image", product.getSvgImagePath())
                .addValue("price", product.getPrice());
        int hasQueryWorked = jdbcTemplate.update("insert into products (id, name, description, image, price) values (:id, :name, :description, :image, :price)", namedParameters);
        if (hasQueryWorked > 0) {
            System.out.println(product.getName() + " has been inserted");
        } else {
            throw new RuntimeException("failed to insert product");
        }
    }

    public void updateProduct(Model.domain.Product product) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", product.getId())
                .addValue("name", product.getName())
                .addValue("description", product.getDescription())
                .addValue("image", product.getSvgImagePath())
                .addValue("price", product.getPrice());
        int hasQueryWorked = jdbcTemplate.update("update products set name=:name,description=:description,image=:image,price=:price where id = :id", namedParameters);
        if (hasQueryWorked == 0) {
            throw new RuntimeException("failed to update product");
        }
    }

    public boolean deleteProduct(int id) {
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("id", id);
        int hasQueryWorked = jdbcTemplate.update("delete from products where id=:id", namedParameters);
        if (hasQueryWorked > 0) {
            return true;
        }
        return false;
    }

    public Model.domain.Product getProduct(int id) {
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("id", id);
        try {
            return jdbcTemplate.query("select id, name, description, image, price from products where id=:id", namedParameters, (resultSet, i) -> {
                return makeProductList(resultSet, id);
            }).get(0);
        } catch (DataAccessException | IndexOutOfBoundsException e) {
            throw new RuntimeException("failed to find product", e);
        }
    }

    public List<Model.domain.Product> findProducts(String search) {
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("query", search);
        try {
            return jdbcTemplate.query("select id, name, description, image, price from products where name like CONCAT('%', CONCAT(:query, '%')) or description like CONCAT('%', CONCAT(:query, '%'))",
                    namedParameters, (resultSet, i) -> {
                        int id = resultSet.getInt("id");
                        return makeProductList(resultSet, id);
                    });
        } catch (DataAccessException e) {
            throw new RuntimeException("failed to find product", e);
        }
    }

    private Model.domain.Product makeProductList(ResultSet resultSet, int id) throws SQLException {
        String name = resultSet.getString("name");
        String description = resultSet.getString("description");
        String svgImagePath = resultSet.getString("image");
        double price = resultSet.getDouble("price");
        return productTransformer.toDomain(new Product.ProductBuilder().setDescription(description)
                .setId(id)
                .setName(name)
                .setPrice(price)
                .setSvgImagePath(svgImagePath)
                .createProduct());
    }
}
