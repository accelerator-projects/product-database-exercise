package Facade;

import Model.ProductTransformer;
import Model.json.Product;
import Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/product")
public class ProductFacade {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductTransformer productTransformer;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Product> getProduct(@PathVariable(value = "id") final int id) {
        return new ResponseEntity<>(productTransformer.toJson(productService.getProduct(id)), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteProduct(@PathVariable(value = "id") final int id) {
        boolean success = productService.deleteProduct(id);
        if (success) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ProductError> updateProduct(@PathVariable(value = "id") final int id,
            @RequestBody final Product product) {
        try {
            productService.updateProduct(id, productTransformer.toDomain(product));
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(new ProductError("id in path didn't match id in product"),
                    HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/search/{query}", method = RequestMethod.POST)
    public ResponseEntity<List<Product>> search(@PathVariable(value = "query") final String query) {
        List<Product> products = productService.findProducts(query).stream()
                .map(product -> productTransformer.toJson(product)).collect(Collectors.toList());
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<ProductError> createProduct(@RequestBody final Product product) {
        try {
            productService.createProduct(productTransformer.toDomain(product));
        } catch (RuntimeException e) {
            return new ResponseEntity<>(new ProductError("product already exists with given id"),
                    HttpStatus.BAD_REQUEST);
        }
        
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
