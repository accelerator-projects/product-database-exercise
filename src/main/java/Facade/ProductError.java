package Facade;

public class ProductError {
    private String message;

    public ProductError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
