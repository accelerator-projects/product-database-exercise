package Service;

import java.util.List;

import org.springframework.stereotype.Service;

import Model.domain.Product;
import Repository.ProductDatabase;

@Service
public class ProductService {
    private final ProductDatabase productDatabase;

    public ProductService(final ProductDatabase productDatabase) {
        this.productDatabase = productDatabase;
    }

    public Product getProduct(final int id) {
        return productDatabase.getProduct(id);
    }

    public boolean deleteProduct(final int id) {
        return productDatabase.deleteProduct(id);
    }

    public void updateProduct(final int id, final Product product) {
        productDatabase.getProduct(id);
        productDatabase.updateProduct(product);
    }

    public List<Product> findProducts(final String query) {
        return productDatabase.findProducts(query);
    }

    public void createProduct(final Product product) {
        productDatabase.createProduct(product);
    }
}