package Model;

import Model.domain.Product;

public class ProductTransformer {
    public Product toDomain(final Model.json.Product json) {
        return new Product(json.getId(),
        json.getName(),
        json.getDescription(),
        json.getSvgImagePath(),
        json.getPrice());
    }

    public Model.json.Product toJson(final Product domain) {
        return new Model.json.Product(domain.getId(),
        domain.getName(),
        domain.getDescription(),
        domain.getSvgImagePath(),
        domain.getPrice());
    }
}