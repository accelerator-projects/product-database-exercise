package Model.domain;

import java.util.Objects;

public class Product {
    private final long id;
    private final String name;
    private final String description;
    private final String svgImagePath;
    private final double price;

    public Product(final long id, final String name, final String description, final String svgImagePath,
            final double price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.svgImagePath = svgImagePath;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getSvgImagePath() {
        return svgImagePath;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name='" + name + '\'' + ", description='" + description + '\''
                + ", svgImagePath='" + svgImagePath + '\'' + ", price=" + price + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Product product = (Product) o;
        return id == product.id && Double.compare(product.price, price) == 0 && Objects.equals(name, product.name)
                && Objects.equals(description, product.description)
                && Objects.equals(svgImagePath, product.svgImagePath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, svgImagePath, price);
    }

    public static class ProductBuilder {
        private long id;
        private String name;
        private String description;
        private String svgImagePath;
        private double price;

        public ProductBuilder setId(long id) {
            this.id = id;
            return this;
        }

        public ProductBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public ProductBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public ProductBuilder setSvgImagePath(String svgImagePath) {
            this.svgImagePath = svgImagePath;
            return this;
        }

        public ProductBuilder setPrice(double price) {
            this.price = price;
            return this;
        }

        public Product createProduct() {
            return new Product(id, name, description, svgImagePath, price);
        }
    }
}
