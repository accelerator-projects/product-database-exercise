-- noinspection SqlNoDataSourceInspectionForFile

-- noinspection SqlDialectInspectionForFile

create database library;
\connect library;

CREATE TABLE products (
 id BIGINT PRIMARY KEY,
 name VARCHAR (50) NOT NULL,
 description VARCHAR (5000) NOT NULL,
 image VARCHAR (355) NOT NULL,
 price INT NOT NULL
);