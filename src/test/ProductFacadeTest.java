import Facade.ProductError;
import Facade.ProductFacade;
import Model.Product;
import Repository.ProductDatabase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ProductFacadeTest {
    @Mock
    private ProductDatabase productDatabase;

    @InjectMocks
    private ProductFacade productFacade;

    @Before
    public void setUp() {
        productFacade = new ProductFacade();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void canSuccessfullyReturnProduct() {
        Product product = new Product.ProductBuilder().setId(1)
                .setName("A")
                .setDescription("B")
                .setSvgImagePath("C")
                .setPrice(2)
                .createProduct();
        when(productDatabase.getProduct(1)).thenReturn(Optional.of(product));
        assertEquals(productFacade.getProduct(1),new ResponseEntity<>(product, HttpStatus.OK));
    }

    @Test
    public void canFailToReturnProduct() {
        when(productDatabase.getProduct(1)).thenReturn(Optional.empty());
        assertEquals(productFacade.getProduct(1),new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Test
    public void canSuccessfullyDeleteProduct() {
        when(productDatabase.deleteProduct(1)).thenReturn(true);
        assertEquals(productFacade.deleteProduct(1),new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @Test
    public void canFailToDeleteProduct() {
        when(productDatabase.deleteProduct(1)).thenReturn(false);
        assertEquals(productFacade.deleteProduct(1),new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Test
    public void canSuccessfullyUpdateProduct() {
        Product product = new Product.ProductBuilder().setId(1)
                .setName("A")
                .setDescription("B")
                .setSvgImagePath("C")
                .setPrice(2)
                .createProduct();
        doNothing().when(productDatabase).updateProduct(product);
        assertEquals(productFacade.updateProduct(1,product),new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @Test
    public void canFailToUpdateProduct() {
        Product product = new Product.ProductBuilder().setId(1)
                .setName("A")
                .setDescription("B")
                .setSvgImagePath("C")
                .setPrice(2)
                .createProduct();
        assertEquals(productFacade.updateProduct(2,product).getStatusCode(),HttpStatus.BAD_REQUEST);
    }

    @Test
    public void canSearchForProductByQuery() {
        Product product = new Product.ProductBuilder().setId(1)
                .setName("A")
                .setDescription("B")
                .setSvgImagePath("C")
                .setPrice(2)
                .createProduct();
        List<Product> productList = new LinkedList<>();
        productList.add(product);
        when(productDatabase.findProducts("B")).thenReturn(productList);
        assertEquals(productFacade.search("B"),new ResponseEntity<>(productList, HttpStatus.OK));
    }

    @Test
    public void canSuccessfullyCreateProduct() {
        Product product = new Product.ProductBuilder().setId(1)
                .setName("A")
                .setDescription("B")
                .setSvgImagePath("C")
                .setPrice(2)
                .createProduct();
        doNothing().when(productDatabase).createProduct(product);
        assertEquals(productFacade.createProduct(product),new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @Test
    public void canFailToCreateProduct() {
        Product product = new Product.ProductBuilder().setId(1)
                .setName("A")
                .setDescription("B")
                .setSvgImagePath("C")
                .setPrice(2)
                .createProduct();
        doThrow(new RuntimeException()).when(productDatabase).createProduct(product);
        assertEquals(productFacade.createProduct(product).getStatusCode(),HttpStatus.BAD_REQUEST);
    }
}
