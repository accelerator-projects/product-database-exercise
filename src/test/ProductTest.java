import Model.Product;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple App.
 */
public class ProductTest
{
    @Test
    public void productCanBeInitialised()
    {
       Product product = new Product.ProductBuilder().setId(1)
               .setName("A")
               .setDescription("B")
               .setSvgImagePath("C")
               .setPrice(2)
               .createProduct();
        assertTrue( product instanceof Product );
    }
    Product product = new Product.ProductBuilder().setId(1)
            .setName("A")
            .setDescription("B")
            .setSvgImagePath("C")
            .setPrice(2)
            .createProduct();
    @Test
    public void productGetFunctionsWork() {
        assertEquals(product.getDescription(),"B");
        assertEquals(product.getId(),1);
        assertEquals(product.getName(),"A");
        assertEquals(product.getPrice(),2,0.01);
        assertEquals(product.getSvgImagePath(),"C");
    }
    @Test
    public void productPrintsToStringCorrectly() {
        assertEquals(product.toString(),"Product{id=1, name='A', description='B', svgImagePath='C', price=2.0}");
    }
    @Test
    public void productsCanBeCompared() {
        Product productCopy = new Product.ProductBuilder().setId(1)
                .setName("A")
                .setDescription("B")
                .setSvgImagePath("C")
                .setPrice(2)
                .createProduct();
        assertTrue(product.equals(productCopy));
    }
}
