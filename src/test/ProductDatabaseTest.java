import Model.Product;
import Repository.ProductDatabase;
import Repository.ProductLoader;
import org.junit.Before;
import org.junit.Test;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ProductDatabaseTest {
    @Mock
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Mock
    private ProductLoader productLoader;

    private ProductDatabase productDatabase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        productDatabase = new ProductDatabase(jdbcTemplate, productLoader);
    }

    public class DatabaseMock {

        public List<Product> productList = new LinkedList<>();

        public List<Product> getProductList() {
            return productList;
        }

        public void addProductToList(Product product) {
            productList.add(product);
        }

        public void removeProductFromList(Product product) {
            productList.remove(product);
        }

        public boolean isEmpty() {
            return productList.size() == 0;
        }

    }

    @Test
    public void canAddToDatabase() {
        Product product = new Product.ProductBuilder().setId(1)
                .setName("A")
                .setDescription("B")
                .setSvgImagePath("C")
                .setPrice(2)
                .createProduct();
        DatabaseMock databaseMock = new DatabaseMock();
        when(jdbcTemplate.update(anyString(), any(SqlParameterSource.class))).thenAnswer(i -> {
            databaseMock.addProductToList(product);
            return 1;
        });
        productDatabase.createProduct(product);
        assertEquals(databaseMock.getProductList().get(0), product);
    }

    @Test
    public void canUpdateProductInDatabase() {
        Product product = new Product.ProductBuilder().setId(1)
                .setName("A")
                .setDescription("B")
                .setSvgImagePath("C")
                .setPrice(2)
                .createProduct();
        Product updatedProduct = new Product.ProductBuilder().setId(1)
                .setName("B")
                .setDescription("C")
                .setSvgImagePath("D")
                .setPrice(3)
                .createProduct();
        DatabaseMock databaseMock = new DatabaseMock();
        databaseMock.addProductToList(product);
        when(jdbcTemplate.update(anyString(), any(SqlParameterSource.class))).thenAnswer(i -> {
            databaseMock.removeProductFromList(product);
            databaseMock.addProductToList(updatedProduct);
            return 1;
        });
        productDatabase.updateProduct(updatedProduct);
        assertEquals(databaseMock.getProductList().get(0), updatedProduct);
    }

    @Test
    public void canDeleteAProduct() {
        Product product = new Product.ProductBuilder().setId(1)
                .setName("A")
                .setDescription("B")
                .setSvgImagePath("C")
                .setPrice(2)
                .createProduct();
        DatabaseMock databaseMock = new DatabaseMock();
        databaseMock.addProductToList(product);
        when(jdbcTemplate.update(anyString(), any(SqlParameterSource.class))).thenAnswer(i -> {
            databaseMock.removeProductFromList(product);
            return 1;
        });
        productDatabase.deleteProduct(1);
        assertTrue(databaseMock.isEmpty());
    }

    @Test
    public void canReadAProduct() {
        Product product = new Product.ProductBuilder().setId(1)
                .setName("A")
                .setDescription("B")
                .setSvgImagePath("C")
                .setPrice(2)
                .createProduct();
        DatabaseMock databaseMock = new DatabaseMock();
        databaseMock.addProductToList(product);
        when(jdbcTemplate.query(anyString(), any(SqlParameterSource.class), any(RowMapper.class))).thenAnswer(i -> {
            List<Product> productList = new LinkedList<>();
            productList.add(databaseMock.getProductList().get(0));
            return productList;
        });
        assertEquals(productDatabase.getProduct(1).get(), product);
    }

    @Test
    public void canFindAProduct() {
        Product product = new Product.ProductBuilder().setId(1)
                .setName("A")
                .setDescription("B")
                .setSvgImagePath("C")
                .setPrice(2)
                .createProduct();
        DatabaseMock databaseMock = new DatabaseMock();
        databaseMock.addProductToList(product);
        when(jdbcTemplate.query(anyString(), any(SqlParameterSource.class), any(RowMapper.class))).thenAnswer(i -> {
            List<Product> productList = new LinkedList<>();
            Product item = databaseMock.getProductList().get(0);
            SqlParameterSource sources = (SqlParameterSource) i.getArguments()[1];
            if(item.getDescription().contains(sources.getValue("query").toString()) || item.getName().contains(sources.getValue("query").toString())) {
                productList.add(databaseMock.getProductList().get(0));
            }
            return productList;
        });
        assertEquals(productDatabase.findProducts("B").get(0), product);
    }
}
